#__________________________________________________________________________
#Temporal dynamics of mycorrhizal fungal communities and co-associations with grassland plant communities following 
#experimental manipulation of rainfall  

#Coline Deveautour, Sally A. Power, Kirk L. Barnett, Raul Ochoa-Hueso, Suzanne Donn, Alison E. Bennett, Jeff R. Powell

#__________________________________________________________________________


#_______________________________
#Co-Correspondance analysis with plant and AM fungal communities march 2014 to march 2017 in DRI-Grass
#Analysis on plant community composition response to altered rainfall
#_______________________________


#_______________________________
#Data preparation 
source('scripts/prepData.R')

#load libraries
library(vegan)
library(cocorresp)
library(labdsv)
library(doBy)

#Prepare both plant and soil matrices for the Co-correspondance analyses

#Discard spring-2015 from soil.data because there is no corresponding data in the plant table
all(rownames(soil.mat.otu) == soil.data$Sample)
soil.mat.otu<-with(soil.data, subset(soil.mat.otu, soil.data$Season.Year!='spring-2015'))
soil.data<-subset(soil.data, Season.Year!='spring-2015')
soil.data<-droplevels(soil.data)

#Matrices in the same order?
all(rownames(soil.mat.otu) == plant.data$soilID)

#Data frame with plant data only (exclude treatments)
plantcom.df<-plant.data[,8:50]
#Delete absent species 
plantcom.df<-plantcom.df[,colSums(plantcom.df)!=0]

#Delete sample with no plants (CD61) in both matrices 
plantcom.df<-plantcom.df[rowSums(plantcom.df)!=0,]
plant.data<-plant.data[plant.data$soilID!='CD61',]
soil.mat.otu<-soil.mat.otu[rownames(soil.mat.otu)!='CD61',]
soil.data<-soil.data[soil.data$Sample!='CD61',]

#convert soil.mat.otu into a presence/absence OTU matrix
soil.mat.otu[soil.mat.otu>0] <-1
soil.mat.otu<-as.data.frame(soil.mat.otu)

#Plant data transformation
#Reduce asymmetry of species distribution in plants using sqrt transformation
plantcom.df<-sqrt(plantcom.df)


#_______________________________
#Analyses plant community composition responses to rainfall

#PerMANOVA
adonis(vegdist(plantcom.df, method = 'bray') ~ treatment + Season.Year, plant.data)

#RDA constrained by watering treatment
pca<-rda(plantcom.df ~ treatment, plant.data, scale=T)
pca
summary(pca)[['cont']][['importance']]

#Plot
palette(c('steelblue1','chocolate1','firebrick'))
with(plant.data, plot(scores(pca, display = 'sites'), col=treatment, pch=16, xlab='RD Axis 1 (2.8%)', ylab='RD Axis 2 (1.3%)'))
legend('topright', levels(plant.data$treatment), pch=16, col= palette(),title = 'Watering treatment', cex=0.9, bty='n')
#_______________________________


#_______________________________
#Co-Correspondance analyses
#_______________________________
 

#_______________________________
#Analyses at each sampling date 

#Subset data frames by sampling dates
data.split<-split(soil.data, soil.data$Season.Year)
new_names <- paste('soil.data',as.character(levels(soil.data$Season.Year)), sep='.')
for (i in 1:length(data.split)) {
  assign(new_names[i], data.split[[i]])
}
rm(data.split, new_names)

data.split<-with(soil.data, split(soil.mat.otu, soil.data$Season.Year))
new_names <- paste('soil.mat',as.character(levels(soil.data$Season.Year)), sep='.')
for (i in 1:length(data.split)) {
  assign(new_names[i], data.split[[i]])
}
rm(data.split, new_names)

data.split<-with(soil.data, split(plantcom.df, soil.data$Season.Year))
new_names <- paste('plant.mat',as.character(levels(soil.data$Season.Year)), sep='.')
for (i in 1:length(data.split)) {
  assign(new_names[i], data.split[[i]])
}
rm(data.split, new_names)

#_______________________________
#Symmetric Co-CA at each sampling date
#_______________________________


#Autumn 2014
fp.aut14.sym<-coca(`soil.mat.autumn-2014`~.,data=`plant.mat.autumn-2014`, method='symmetric', n.axes = 4)
fp.aut14.sym #Percentage of fit = "explained" / "total" * 100 

#Cross-correlation between axes (measure of association between predictor and response matrices)
corAxis(fp.aut14.sym) 

##permutation test, significance of the % of fit
#create empty list vector where outputs are going to be stored
output<-vector(mode='list', length = 500)

#First output is based on 'real' data
output[[1]]<-summary(fp.aut14.sym, display=F)

#loop to extract symmetric Co-CA inertias from matrices with shuffled rows 
# /!\ WARNING /!\ This might take some time to run
for (i in 2:500){
  perm.fungi <- `soil.mat.autumn-2014`[sample(nrow(`soil.mat.autumn-2014`)),]
  perm.plant <- `plant.mat.autumn-2014`[sample(nrow(`plant.mat.autumn-2014`)),]
  x <-coca(perm.fungi~.,data=perm.plant, method='symmetric', n.axes = 4)
  output[[i]]<-summary(x, display=F)
  rm(x, perm.fungi, perm.plant)
}

#Create the funtion to obtain the percentage of fit
perc.fit<-function(x){
  fit<- 1 - (x$inertia$residual$Y / x$inertia$total$Y)
  return(fit)}

#Obtain the percentage of fit for every element of the list
fit.perm<-sapply(output, perc.fit)
#Obtain the p-value
mean(fit.perm >= fit.perm[1])
#_______________________________


#Run the symmetric test at each sampling date and repeat the code above for each date
fp.spr14.sym<-coca(`soil.mat.spring-2014`~.,data=`plant.mat.spring-2014`, method='symmetric', n.axes = 4)
fp.aut15.sym<-coca(`soil.mat.autumn-2015`~.,data=`plant.mat.autumn-2015`, method='symmetric', n.axes = 4)
fp.aut16.sym<-coca(`soil.mat.autumn-2016`~.,data=`plant.mat.autumn-2016`, method='symmetric', n.axes = 4)
fp.spr16.sym<-coca(`soil.mat.spring-2016`~.,data=`plant.mat.spring-2016`, method='symmetric', n.axes = 4)
fp.aut17.sym<-coca(`soil.mat.autumn-2017`~.,data=`plant.mat.autumn-2017`, method='symmetric', n.axes = 4)


#_______________________________
#Which plants are strongly co-associated with which fungi?
#_______________________________

#Autumn 2014
#Extract the species loadings of the second axis
sp.X.aut14<-as.data.frame(scores(fp.aut14.sym$scores$species$X[,'COCA 2'])); sp.X.aut14$species<-rownames(sp.X.aut14)
sp.Y.aut14<-as.data.frame(scores(fp.aut14.sym$scores$species$Y[,'COCA 2'])); sp.Y.aut14$species<-rownames(sp.Y.aut14)

#obtain species with 'extreme' loadings values
sp.X.aut14[which(sp.X.aut14$Dim1 == min(sp.X.aut14$Dim1)),'species'] #AMF
sp.Y.aut14[which(sp.Y.aut14$Dim1 == min(sp.Y.aut14$Dim1)),'species'] #plant species

sp.X.aut14[which(sp.X.aut14$Dim1 == max(sp.X.aut14$Dim1)),'species'] #AMF
sp.Y.aut14[which(sp.Y.aut14$Dim1 == max(sp.Y.aut14$Dim1)),'species'] #plant species

#Graphs are modified to read labels and see associations
plot(fp.aut14.sym, which='predictor', type = 'n', xlim=c(-2,0), ylim = c(-9,0))
text(fp.aut14.sym$scores$species$X[,1],fp.aut14.sym$scores$species$X[,2], rownames(fp.aut14.sym$scores$species$X), cex=0.8)
#Commelina has "extreme" loadings values

#Extract the OTUs associated with Commelina
sp.Y.aut14<-as.data.frame(scores(fp.aut14.sym$scores$species$Y[,2])); sp.Y.aut14$species<-rownames(sp.Y.aut14) #Extract OTUs and loading scores along the second axis
sp.Y.aut14[which(sp.Y.aut14$Dim1 < -6),] #Find AMF associated with Commelina
Ccya_assocAut<-sp.Y.aut14[which(sp.Y.aut14$Dim1 < -6),] #Extract those AMF associated

#get the sequence to identify the OTU
getSequence(fas[c(which(names(fas) %in% Ccya_assocAut$species))], as.string=T)

#Spring 2014 (repeat the same logic to the other dates)
plot(fp.spr14.sym, which='predictor', type = 'n')
text(fp.spr14.sym$scores$species$X[,1],fp.spr14.sym$scores$species$X[,2], rownames(fp.spr14.sym$scores$species$X), cex=0.8)

#Extract the OTU associated with Commelina
sp.Y.spr14<-as.data.frame(scores(fp.spr14.sym$scores$species$Y[,2])); sp.Y.spr14$species<-rownames(sp.Y.spr14)
sp.Y.spr14[which(sp.Y.spr14$Dim1 < -4),] #AMF assoc with comm cya
Ccya_assocSpr<-sp.Y.spr14[which(sp.Y.spr14$Dim1 < -4),]

#Extract AMF associated with Commelina cyanea at both time points (autumn and spring 2014)
commonCcya<-Ccya_assocSpr[match(Ccya_assocAut$species,Ccya_assocSpr$species),]
commonCcya<-commonCcya[complete.cases(commonCcya),]
#"ITSall_OTUa_8031" "ITSall_OTUa_1892" "ITSall_OTUa_2888" "ITSall_OTUa_1411"

#Autumn 2015
plot(fp.aut15.sym, which='predictor', type = 'n', xlim = c(-5,2))
text(fp.aut15.sym$scores$species$X[,1],fp.aut15.sym$scores$species$X[,2], rownames(fp.aut15.sym$scores$species$X), cex=0.8)

#AMF species associated with Sida rhombifolia
sp.Y.aut15<-as.data.frame(scores(fp.aut15.sym$scores$species$Y[,1])); sp.Y.aut15$species<-rownames(sp.Y.aut15)
sp.Y.aut15[which(sp.Y.aut15$Dim1 < -2),] #AMF
Sida_assoc15<-sp.Y.aut15[which(sp.Y.aut15$Dim1 < -2),]

#AMF associated with Commelina cyanea in autumn 2015
sp.Y.aut15<-as.data.frame(scores(fp.aut15.sym$scores$species$Y[,2])); sp.Y.aut15$species<-rownames(sp.Y.aut15)
sp.Y.aut15[which(sp.Y.aut15$Dim1 < -3),] #AMF
Ccya_assoc15<-sp.Y.aut15[which(sp.Y.aut15$Dim1 < -3),]

#common AMF associated with Commelina cyanea at other dates?
match(Ccya_assocAut$species,Ccya_assoc15$species) #No shared spp between 2015 and aut2014
match(Ccya_assocSpr$species,Ccya_assoc15$species) #No shared spp between 2015 and spr2014


#Autumn 2016
plot(fp.aut16.sym, which='predictor', type = 'n', xlim = c(-4,2))
text(fp.aut16.sym$scores$species$X[,1],fp.aut16.sym$scores$species$X[,2], rownames(fp.aut16.sym$scores$species$X), cex=0.8)
#Sida has "extreme" loading values

#AMF species associated with Sida rhombifolia
sp.Y.aut16<-as.data.frame(scores(fp.aut16.sym$scores$species$Y[,2])); sp.Y.aut16$species<-rownames(sp.Y.aut16)
sp.Y.aut16[which(sp.Y.aut16$Dim1 < -4),] #AMF
Sida_assoc16<-sp.Y.aut16[which(sp.Y.aut16$Dim1 < -3),]

#common AM fungi associated with Sida in autumn 2015 and autumn 2016?
commonSida<-Sida_assoc16[match(Sida_assoc15$species, Sida_assoc16$species),]
commonSida<-commonSida[complete.cases(commonSida),]

#Are the spp assoc with Sida present at any other date?
match(Sida_assoc15$species,rownames(fp.aut14.sym$scores$species$Y))
match(Sida_assoc15$species,rownames(fp.spr14.sym$scores$species$Y))
match(Sida_assoc15$species,rownames(fp.spr16.sym$scores$species$Y))

match(Sida_assoc16$species,rownames(fp.aut14.sym$scores$species$Y))
match(Sida_assoc16$species,rownames(fp.spr14.sym$scores$species$Y))
match(Sida_assoc16$species,rownames(fp.spr16.sym$scores$species$Y)) #Yes

#Are they in the same plot or in other plots?
#2015
match(Sida_assoc15$species, colnames(`soil.mat.autumn-2014`))
#subset the OTUs assoc with Sida
sida15<-soil.mat.otu[ , match(Sida_assoc15$species, colnames(soil.mat.otu))] 
all(rownames(sida15)==soil.data$Sample) #make sure both data frames are in same order

#Number of OTUs assoc with Sida in 2015 that are present at other dates and plots
summaryBy(rowSums(sida15) ~ Plot + Season.Year, data=soil.data, FUN=sum) 

#2016
sida16<-soil.mat.otu[ , match(Sida_assoc16$species, colnames(soil.mat.otu))] 
all(rownames(sida16)==soil.data.otu$Sample) #make sure both data frames are in same order

#Number of OTUs assoc with Sida in 2016 that are present at other dates and plots
summaryBy(rowSums(sida16) ~ Plot + Season.Year, data=soil.data, FUN=sum) 


#Spring 2016
plot(fp.spr16.sym, which='predictor', type = 'n', xlim = c(-2,-10))
text(fp.spr16.sym$scores$species$X[,1]*2,fp.spr16.sym$scores$species$X[,2]*2, rownames(fp.spr16.sym$scores$species$X), cex=0.8)

#plant species 
sp.X.spr16<-as.data.frame(scores(fp.spr16.sym$scores$species$X[,1])); sp.X.spr16$species<-rownames(sp.X.spr16)
sp.X.spr16[which(sp.X.spr16$Dim1 < -2),] #AMF


#______________________________
#Indicator OTUs
#______________________________

#Indicator OTUs for autumn 2014
ind.trt.aut14 <- indval(`soil.mat.autumn-2014`[,colSums(`soil.mat.autumn-2014`)!=0], as.numeric(`soil.data.autumn-2014`$Treatment))
summary(ind.trt.aut14)

#loadings of the indicator OTU
fp.aut14.sym$scores$species$Y['ITSall_OTUa_182',c('COCA 1','COCA 2')] #these loadings are used in the symmetric coca plots


#Indicator OTUs for spring 2014
ind.trt.spr14 <- indval(`soil.mat.spring-2014`[,colSums(`soil.mat.spring-2014`)!=0], as.numeric(`soil.data.spring-2014`$Treatment))
summary(ind.trt.spr14)

#loadings of the indicator OTU
fp.spr14.sym$scores$species$Y['ITSall_OTUa_174',c('COCA 1','COCA 2')]

#Indicator OTUs for autumn 2015
ind.trt.aut15 <- indval(`soil.mat.autumn-2015`[,colSums(`soil.mat.autumn-2015`)!=0], as.numeric(`soil.data.autumn-2015`$Treatment))
summary(ind.trt.aut15)

#loadings of the indicator OTU
fp.aut15.sym$scores$species$Y[c('ITSall_OTUa_217','ITSall_OTUb_337') ,c('COCA 1','COCA 2')]


#Indicator OTUs for autumn 2016
ind.trt.aut16 <- indval(`soil.mat.autumn-2016`[,colSums(`soil.mat.autumn-2016`)!=0], as.numeric(`soil.data.autumn-2016`$Treatment))
summary(ind.trt.aut16)

#loadings of the indicator OTU
fp.aut16.sym$scores$species$Y[c('ITSall_OTUa_63') ,c('COCA 1','COCA 2')]


#Indicator OTUs for spring 2016
ind.trt.spr16 <- indval(`soil.mat.spring-2016`[,colSums(`soil.mat.spring-2016`)!=0], as.numeric(`soil.data.spring-2016`$Treatment))
summary(ind.trt.spr16)

#loadings of the indicator OTU
fp.spr16.sym$scores$species$Y[c('ITSall_OTUf_2334','ITSall_OTUa_5600','ITSall_OTUb_3613','ITSall_OTUa_8605','ITSall_OTUf_1440',
                                'ITSall_OTUf_565','ITSall_OTUe_5958', 'ITSall_OTUf_869','ITSall_OTUf_2467','ITSall_OTUa_63',
                                'ITSall_OTUb_480','ITSall_OTUb_261','ITSall_OTUe_6690') ,c('COCA 1','COCA 2')]

#Indicator OTUs for autumn 2017
ind.trt.aut17 <- indval(`soil.mat.autumn-2017`[,colSums(`soil.mat.autumn-2017`)!=0], as.numeric(`soil.data.autumn-2017`$Treatment))
summary(ind.trt.aut17)


#_______________________________
#Predictive Co-CA
#_______________________________

#plants predicting AM fungi
#______________________________
#Plants predict the AM fungal community only in spring 2014
fp.spr14.pred<-coca(`soil.mat.spring-2014`~.,data=`plant.mat.spring-2014`, method='predictive')  
fp.spr14.pred

#Cross-validation (leave-one-out analysis; accuracy of prediction, % of fit)
crossval(`soil.mat.spring-2014`, `plant.mat.spring-2014`) #values above 0 validates the model; below 0, prediction is worse than expected under null model
permutest(fp.aut14.pred, permutations = 99) #assess significant axes


#AMF predicting plants
#______________________________

fp.aut14.pred<-coca(`plant.mat.autumn-2014`~.,data=`soil.mat.autumn-2014`, method='predictive') 
fp.aut14.pred #45% variation explained in plants with 2 first axis ; 22.5% in AMF

#Cross-validation (leave-one-out analysis; accuracy of prediction, % of fit)
crossval(`plant.mat.autumn-2014`, `soil.mat.autumn-2014`)

#Repeat same code for other dates
crossval(`plant.mat.spring-2014`, `soil.mat.spring-2014`) #and following dates

#______________________________
#Plots for symmetric Co-CA
#______________________________
#AMF and plant plots in 5 panels 

#To export the figure
#pdf('symCoca.pdf', width=7.05, height=13)
#run code for plots
#dev.off()

#Autumn 2014
l<-layout(matrix(c(1,2,3,4,5,6,7,8,9,10,11,11), ncol=2, byrow=TRUE), heights=c(4,4,4,4,4,1))
#layout.show(l)

#Plot the response community (fungal community)
par(mar=c(4, 6, 1, 1))
palette(c('steelblue1','chocolate1','firebrick'))

plot(fp.aut14.sym, which='response', type='n', main='AM fungal community', xlim=c(-3,3), ylim=c(-8.5,2.5))
text(-3, -7.5, '(a)', cex=1.2, font=2)
mtext('Autumn 2014', side=2, line=4.5, font=2)
points(fp.aut14.sym$scores$species$Y, col='grey',pch=4)
points(fp.aut14.sym$scores$species$Y['ITSall_OTUa_182','COCA 1'],fp.aut14.sym$scores$species$Y['ITSall_OTUa_182','COCA 2'],col='black', bg='black', pch=25) #indicator OTU
with(`soil.data.autumn-2014`, points(fp.aut14.sym$scores$site$Y, col=Treatment, pch=16))

#Plot the predictor com (plant)
par(mar=c(4, 4, 1, 2))
plot(fp.aut14.sym, which='predictor', type='n', main='Plant community', xlim=c(-3,3), ylim=c(-8.5,2.5))
text(-3, -7.5, '(b)', cex=1.2, font=2)
points(fp.aut14.sym$scores$species$X, col='grey', pch=4)
with(`soil.data.autumn-2014`, points(fp.aut14.sym$scores$site$X, col=Treatment, pch=16))

#Spring 2014
#Plot the response community (fungal community)
par(mar=c(4, 6, 1, 1))
plot(fp.spr14.sym, which='response', type='n', xlim=c(-3,3), ylim=c(-9,4))
text(-3, -7.5, '(c)', cex=1.2, font=2)
mtext('Spring 2014', side=2, line=4.5, font=2)
points(fp.spr14.sym$scores$species$Y, col='grey',pch=4)
points(fp.spr14.sym$scores$species$Y['ITSall_OTUa_174','COCA 1'],fp.spr14.sym$scores$species$Y['ITSall_OTUa_174','COCA 2'],col='black', bg='black', pch=25) #indicator OTU
with(`soil.data.spring-2014`, points(fp.spr14.sym$scores$site$Y, col=Treatment, pch=16))

#Plot the predictor com (plant)
par(mar=c(4, 4, 1, 2))
plot(fp.spr14.sym, which='predictor', type='n', xlim=c(-3,3), ylim=c(-9,4))
text(-3, -7.5, '(d)', cex=1.2, font=2)
points(fp.spr14.sym$scores$species$X, col='grey', pch=4)
with(`soil.data.spring-2014`, points(fp.spr14.sym$scores$site$X, col=Treatment, pch=16))

#Autumn 2015
#Plot the response community (fungal community)
par(mar=c(4, 6, 1, 1))
plot(fp.aut15.sym, which='response', type='n', xlim=c(-8,1), ylim=c(-4,3))
text(-8, -3.5, '(e)', cex=1.2, font=2)
mtext('Autumn 2015', side=2, line=4.5, font=2)
points(fp.aut15.sym$scores$species$Y, col='grey',pch=4)
points(fp.aut15.sym$scores$species$Y[c('ITSall_OTUa_217','ITSall_OTUb_337') ,c('COCA 1','COCA 2')],col='black', bg='black', pch=25) #indicator OTU
with(`soil.data.autumn-2015`, points(fp.aut15.sym$scores$site$Y, col=Treatment, pch=16))

#Plot the predictor com (plant)
par(mar=c(4, 4, 1, 2))
plot(fp.aut15.sym, which='predictor', type='n',  xlim=c(-8,1), ylim=c(-4,3))
text(-8, -3.5, '(f)', cex=1.2, font=2)
points(fp.aut15.sym$scores$species$X, col='grey', pch=4)
with(`soil.data.autumn-2015`, points(fp.aut15.sym$scores$site$X, col=Treatment, pch=16))


#Autumn 2016
#Plot the response community (fungal community)
par(mar=c(4, 6, 1, 1))
plot(fp.aut16.sym, which='response', type='n', xlim=c(-3,3), ylim=c(-10.5,2.5))
text(-3, -9.5, '(g)', cex=1.2, font=2)
mtext('Autumn 2016', side=2, line=4.5, font=2)
points(fp.aut16.sym$scores$species$Y, col='grey',pch=4)
points(fp.aut16.sym$scores$species$Y['ITSall_OTUa_63','COCA 1'],fp.aut16.sym$scores$species$Y['ITSall_OTUa_63','COCA 2'],col='black', bg='black', pch=25) #indicator OTU
with(`soil.data.autumn-2016`, points(fp.aut16.sym$scores$site$Y, col=Treatment, pch=16))

#Plot the predictor com (plant)
par(mar=c(4, 4, 1, 2))
plot(fp.aut16.sym, which='predictor', type='n', xlim=c(-3,3), ylim=c(-10.5,2.5))
text(-3, -9.5, '(h)', cex=1.2, font=2)
points(fp.aut16.sym$scores$species$X, col='grey', pch=4)
with(`soil.data.autumn-2016`, points(fp.aut16.sym$scores$site$X, col=Treatment, pch=16))

#Spring 2016
#Plot the response community (fungal community)
par(mar=c(4, 6, 1, 1))
plot(fp.spr16.sym, which='response', type='n', xlim=c(-3,3), ylim=c(-3,4.5))
text(-3, -2.15, '(i)', cex=1.2, font=2)
mtext('Spring 2016', side=2, line=4.5, font=2)
points(fp.spr16.sym$scores$species$Y, col='grey',pch=4)
points(fp.spr16.sym$scores$species$Y[c('ITSall_OTUf_2334','ITSall_OTUa_5600','ITSall_OTUb_3613','ITSall_OTUa_8605','ITSall_OTUf_1440',
                                       'ITSall_OTUf_565','ITSall_OTUe_5958', 'ITSall_OTUf_869','ITSall_OTUf_2467','ITSall_OTUa_63',
                                       'ITSall_OTUb_480','ITSall_OTUb_261','ITSall_OTUe_6690') ,c('COCA 1','COCA 2')],col='black', bg='black', pch=25) #indicator OTU
with(`soil.data.spring-2016`, points(fp.spr16.sym$scores$site$Y, col=Treatment, pch=16))

#Plot the predictor com (plant)
par(mar=c(4, 4, 1, 2))
plot(fp.spr16.sym, which='predictor', type='n', xlim=c(-3,3), ylim=c(-3,4.5))
text(-3, -2.15, '(j)', cex=1.2, font=2)
points(fp.spr16.sym$scores$species$X, col='grey', pch=4)
with(`soil.data.spring-2016`, points(fp.spr16.sym$scores$site$X, col=Treatment, pch=16))

#Add legend below the graphs
par(mai=c(0,0,0,0))
plot.new()
legend(x="center", ncol=3,legend=levels(`soil.data.autumn-2014`$Treatment),
       col=palette(),pch=16, title="Watering Treatment")



#______________________________
# clear workspace when done with this script
rm(list=ls())
